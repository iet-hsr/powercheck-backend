[TOC]

# PowerCheck Backend

## URLs

https://api.powercheck.ch

## Repository

https://gitlab.com/iet-hsr/powercheck-backend.git

## Passwords & Logins

All project related logins and passwords are stored in:

`...P_955_9998_Webenergierechner\Webenergierechner_2019\Zugangsdaten-DO-NOT-COMMIT.txt`

# Admin Page

https://api.powercheck.ch/admin

The admin page can be used to set the data of the SQL-DB. 

The new data needs to provided as timeseries.csv file. 
All previous data in the DB gets overwritten by the data of the newly supplied files.

## Password

The password to access the admin page is defined in the Heroku (hosting provider) Project Settings as a variable.

To change it, log in to `dashboard.heroku.com` then go to:

`energierechner >> Settings >> Config Vars >> Reveal Config Vars >> ADMIN_PW`

# Development Server Startup

This App consists of a React frontend App and an Express backend App. 

In a production environment the frontend gets build on server startup and is then delivered by the backend Express app.

In a development environment the frontend has its own webserver and doesn't rely on the backend to run (except for the API calls).

## Backend

The Backend can be started with the following command from inside the root folder:

```
npm run-script start
```

It then runs on port 5000

## Frontend

To start the frontend dev server, first navigate into the `frontend` folder:

```
cd frontend
```

Then start the dev server with:

```
npm run-script start
```

Runs on port 3000

# Production Deployment

To deploy changes to production, simply push the commits to the `heroku-deployment` branch. CD in GitLab is configured to then automatically deploy the branch to heroku.

# Used Frameworks and Libraries

## Express

Express is used for the backend API and to serve the frontend site.

## React

The frontend is a react app, bootstrapped with create react App. More infos further down.

## Internationalization 

The translation files are located here: `frontend/public/locales/`

`react-i18next`is used for text translations. More infos & documentation: https://react.i18next.com

## UI

Main UI-Library is `@material-ui`

## Icons

Custom Icons are converted with `@svgr/cli`:

- place the `.svg` icon in the folder `/resources/icons/`

- run `npm run svgr` and the icons get generated and saved into `/src/icons/`

- Use in components: 

```
react
import MyIcon from './icons/MyIcon';
<MyIcon width="40px" height="40px" />
```


## NeDB

The backend uses the in memory NeDB to store the data accessed by the api.

## Feedback Mail - formspree.io

The feedback form gets sent to formspree.io From there it gets forwarded to whichever mail address is defined in the formspree.io settings.

To change the target mail address:

1. login to https://formspree.io ([see Logins & Passwords](#passwords-&-logins)) and open the settings for the `PowerCheck Feedback` Form. 
2. Change `Target Email` and click `save`.

# Known Bugs

id8: initial capacity gets float errors when changing the max capacity (Chrome)

id10:

# To-dos

- Confirmation dialog when exiting params without calculating
- Elektromobilität as additional consumer (ui is already implemented on the e-mobility branch)
- Optimize backend memory usage. The function populateMemoryDB uses too much ram, it should only use max 500MB. This will get worse with additional data.
- Fullscreen diagram view
- PDF / PNG export functionality
- Mobile UI layout
