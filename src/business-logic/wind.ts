import { average, groupByYears, linear, mergeResult } from './helpers';
import { CompleteRow, getColumnsWithout29FebPartial } from '../db-persistent/read';
import { BasicSqlColumnName, SqlTableName, WindSqlColumnName } from '../db-persistent/constants';
import { ByYearOrSlug } from '../data-model/memDataStructure';

// Specification:
// Done in Backend:
// 1. Die Windgeschwindigkeiten von 17 Standorte werden als erstes mit dem Faktor 1.5 multipliziert (Da die Messstationen in nur 10m höhe sind).
//   Die 8. Station (Bantiger) ist bereits in 100m höhe und wird nicht mit 1.5 multipliziert.
// 2. Danach werden sie wie gehabt mittels der Funktionen, welche die Betriebskennlinie annähert in relative Leistung [0,1] umgerechnet.
// 3. Als nächstes werden die 18 Standorte gleichgewichtet, damit sich ein Standort ergibt.
// Done in Frontend:
// 4. Die Relative leistung wird mit der eingestellten installierten Leistung multipliziert.
// 5. Zum schluss wird ein Korrekturfaktor von 4.5% addiert.

const HEIGHT_CORRECTION_FACTOR = 1.5;
const STATIONS_ALREADY_IN_100m = [7];

// Erste Spalte (x): m/s (mit Faktor 1.5 (höhen-)korrigierte Geschwindigkeiten)
// Zweite Spalte (y): Relative Leistung der Anlage (1 bedeutet Nennleistung).
const characteristicCurve = [
  [0, 0],
  [2, 0],
  [3, 0.015556],
  [4, 0.051111],
  [5, 0.1],
  [6, 0.171111],
  [7, 0.273333],
  [8, 0.411111],
  [9, 0.577778],
  [10, 0.766667],
  [11, 0.915556],
  [12, 0.988889],
  [13, 1],
  [18, 1],
  [19, 0.984444],
  [20, 0.951111],
  [30, 0.533333],
  [30, 0],
  [35, 0],
];

function calculateRelativePower(meassuredWindSpeed: number, indexOfStation: number) {
  const windSpeed_mps = meassuredWindSpeed * (
    STATIONS_ALREADY_IN_100m.includes(indexOfStation) ? 1 : HEIGHT_CORRECTION_FACTOR
  );
  return linear(windSpeed_mps, characteristicCurve);
}

type PartialSqlTable = (Pick<CompleteRow, BasicSqlColumnName.timeStamp | WindSqlColumnName>)[];

export const calculateWind = async (years: number[]): Promise<ByYearOrSlug<number[]>> => {
  const result: ByYearOrSlug<number[]> = {};
  const rowBatchSize = 100000;
  let startRow = 0;

  while (true) {
    const partialSqlTable = <PartialSqlTable>await getColumnsWithout29FebPartial(
      SqlTableName.timeSeries,
      [
        BasicSqlColumnName.timeStamp,
        ...Object.values(WindSqlColumnName),
      ],
      startRow,
      rowBatchSize,
    );

    if (partialSqlTable.length <= 0) {
      break;
    }

    const windValues = groupByYears(
      years,
      partialSqlTable,
    );

    const computedValues = Object.entries(windValues).reduce<ByYearOrSlug<number[]>>((partialResult, [year, yearRows]) => {
      partialResult[parseInt(year)] = yearRows.map(dataPointsOfAllLocations => (
        average(Object.values(dataPointsOfAllLocations).map(calculateRelativePower))
      ));
      return partialResult;
    }, {});

    mergeResult(result, computedValues);

    startRow += rowBatchSize;
  }

  return result;
};

