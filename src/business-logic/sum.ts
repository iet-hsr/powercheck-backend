import { reduceOnDemandSections } from './helpers';
import { ArbitraryDbObjectWithoutData } from '../data-model/memDataStructure';

const sumReducer = (sum: number, val: number) => {
  return sum + val;
};

export const calculateSumValue = async (years: number[], arbitrary: ArbitraryDbObjectWithoutData[]) => {
  return await reduceOnDemandSections(years, arbitrary, sumReducer, () => 0);
};
