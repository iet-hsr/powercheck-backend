import _ from 'lodash';
import { CompleteRow, getColumnsWithout29Feb } from '../db-persistent/read';
import {
  BasicSqlColumnName,
  SqlColumnName,
  SqlTableName,
  WorkloadSqlColumnName,
} from '../db-persistent/constants';
import { getArbitrary, getOnDemand } from '../db-in-memory/read';
import {
  ArbitraryDbObjectWithoutData,
  ArbitrarySectionName,
  ArbitrarySectionsByYearOrSlug,
  ByYearOrSlug,
  SectionsByYearOrSlug,
} from '../data-model/memDataStructure';
import { OnDemandSection } from '../db-in-memory/constants';
import { ArbitrarySection } from '../data-model/sharedTypes';

export const average = (array: number[]) => {
  let sum = array.reduce((partialSum, val) => {
    return partialSum + val;
  }, 0);
  return sum / array.length;
};

export const reduceOnDemandSections = async <T>(
  years: number[],
  arbitraryWithoutData: ArbitraryDbObjectWithoutData[],
  dataReducer: (partial: T, value: number, i: number, data: number[]) => T,
  getInitial: () => T,
): Promise<SectionsByYearOrSlug<T>> => {
  return _.merge(
    await reduceMeasuresSections(years, dataReducer, getInitial),
    await reduceArbitrarySections(arbitraryWithoutData, dataReducer, getInitial),
  );
};

const reduceMeasuresSections = async <T>(
  years: number[],
  dataReducer: (partial: T, value: number, i: number, data: number[]) => T,
  getInitial: () => T,
) => {
  return await Object.keys(OnDemandSection).reduce<Promise<SectionsByYearOrSlug<T>>>(
    async (partial, untypedSectionName) => {
      const sectionName = untypedSectionName as keyof typeof WorkloadSqlColumnName;
      const result = await partial;

      result[sectionName] = {};

      await asyncForEach(years, async (year) => {
        const data = await getOnDemand(sectionName, year);
        result[sectionName][year]
          = data.data.reduce(dataReducer, getInitial());
      });

      return result;
    }, new Promise((resolve) => resolve(<SectionsByYearOrSlug<T>>{})));
};

const reduceArbitrarySections = async <T>(
  arbitraryWithoutData: ArbitraryDbObjectWithoutData[],
  dataReducer: (partial: T, value: number, i: number, data: number[]) => T,
  getInitial: () => T,
) => {
  return await Object.keys(ArbitrarySection).reduce<Promise<ArbitrarySectionsByYearOrSlug<T>>>(
    async (partial, untypedSectionName) => {
      const sectionName = untypedSectionName as ArbitrarySectionName;
      const result = await partial;

      result[sectionName] = {};

      await asyncForEach(
        arbitraryWithoutData.filter((dboWithoutData) => dboWithoutData.type === sectionName),
        async (dboWithoutData) => {
          const { type, slug } = dboWithoutData;
          if (type === sectionName) {
            const dbo = await getArbitrary(type, slug);
            result[sectionName][slug]
              = dbo.data.reduce(dataReducer, getInitial());
          }
        },
      );

      return result;
    }, new Promise((resolve) => resolve(<SectionsByYearOrSlug<T>>{})));
};

export const getColumnGroupedByYears = async (
  years: number[],
  tableName: SqlTableName,
  columnName: Exclude<SqlColumnName, BasicSqlColumnName.timeStamp>,
) => {
  const rawData = <CompleteRow[]>await getColumnsWithout29Feb(
    tableName,
    [BasicSqlColumnName.timeStamp, columnName],
  );
  return years.reduce<{ [key: number]: number[] }>((result, year) => {
    result[year] = getColumnAsArray(columnName, rawData.filter(yearRowFilterFactory(year)));
    return result;
  }, {});
};

export const getSingleColumnAsArray = async (
  tableName: SqlTableName,
  columnName: Exclude<SqlColumnName, BasicSqlColumnName.timeStamp>,
) => {
  const rawData = <CompleteRow[]>await getColumnsWithout29Feb(
    tableName,
    [columnName],
  );
  return getColumnAsArray(columnName, rawData);
};

export const getColumnAsArray = <T extends Exclude<SqlColumnName, BasicSqlColumnName.timeStamp>>(
  columnName: T,
  rawData: { [_ in T]: number }[],
) => rawData.map(row => row[columnName]);


const deleteTimestamp = <TRow extends { [BasicSqlColumnName.timeStamp]: Date }>(
  dataPoint: TRow,
): Omit<TRow, BasicSqlColumnName.timeStamp> => {
  const copy = Object.assign({}, dataPoint);
  delete copy.timestamp;
  return copy;
};

export type GroupedByYears<TRow> = {
  [key: number]: Omit<TRow, BasicSqlColumnName.timeStamp>[]
}

export const groupByYears = <TRow extends { [BasicSqlColumnName.timeStamp]: Date }>(
  years: number[],
  rawData: TRow[],
): GroupedByYears<TRow> => {
  return years.reduce((result, year) => {
    result[year] = rawData.filter(yearRowFilterFactory(year)).map(deleteTimestamp);
    return result;
  }, <GroupedByYears<TRow>>{});
};

const yearRowFilterFactory = (year: number) => (
  <T extends { timestamp: Date }>(row: T) => getYearWithLastZeroMinute(row.timestamp) === year
);

export const getYearWithLastZeroMinute = (date: Date) => {
  if (
    date.getDate() === 1
    && date.getMonth() === 0
    && date.getHours() === 0
    && date.getMinutes() === 0
  ) {
    return date.getFullYear() - 1;
  }
  return date.getFullYear();
};

export function onlyUnique<T>(value: T, index: number, self: T[]) {
  return self.indexOf(value) === index;
}

export async function asyncForEach<T>(
  array: T[],
  callback: (value: T, i: number, array: T[]) => Promise<void>,
) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

export const isInEnumKeys = <TEnum>(token: any, theEnum: TEnum): token is TEnum[keyof TEnum] =>
  Object.keys(theEnum).includes(token);

export const mergeResult = (
  previous: ByYearOrSlug<number[]>,
  newValues: ByYearOrSlug<number[]>,
) => {
  Object.entries(newValues).forEach(([year, value]) => {
    if (previous[parseInt(year)] === undefined) {
      previous[parseInt(year)] = [];
    }
    previous[parseInt(year)].push(...value);
  });
};

export function linear(x: number, curve: number[][]) {
  if (x <= curve[0][0]) {
    return curve[0][1];
  }
  if (x >= curve[curve.length - 1][0]) {
    return curve[curve.length - 1][1];
  }
  const nextGreater = curve.findIndex((entry) => x <= entry[0]);

  const xLower = curve[nextGreater - 1][0];
  const xUpper = curve[nextGreater][0];
  const yLower = curve[nextGreater - 1][1];
  const yUpper = curve[nextGreater][1];

  const xRange = xUpper - xLower;
  const yRange = yUpper - yLower;

  const xRelativeToRange = (x - xLower) / xRange;

  return yLower + xRelativeToRange * yRange;
}
