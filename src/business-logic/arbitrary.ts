import { getArbitraryWithData, getArbitraryWithoutData } from '../db-persistent/read';
import { ArbitraryDbObject, ArbitraryDbObjectWithoutData } from '../data-model/memDataStructure';

export async function getArbitraryForMemoryDB(): Promise<ArbitraryDbObject[]> {
  const rows = await getArbitraryWithData();
  return rows.map((row) => {
    const { slug, section, data_series, order_weight } = row;
    return {
      type: section,
      slug,
      order: order_weight,
      data: data_series,
    };
  });
}

export async function getArbitraryWithoutDataForMemoryDB(): Promise<ArbitraryDbObjectWithoutData[]> {
  const rows = await getArbitraryWithoutData();
  return rows.map((row) => {
    const { slug, section, order_weight } = row;
    return {
      type: section,
      slug,
      order: order_weight,
    };
  });
}
