import { getTimestampsOfSecondDayOfYear } from '../db-persistent/read';
import { tablesExist } from '../db-persistent/check';
import { updateOrInsert } from '../db-in-memory/write';
import {
  asyncForEach,
  getColumnGroupedByYears,
  getSingleColumnAsArray,
  getYearWithLastZeroMinute,
  onlyUnique,
} from './helpers';
import { calculateWind } from './wind';
import { calculateSolarDiffuse, calculateSolarDirect } from './solar';
import { calculateLowRes } from './lowRes';
import { calculateMaxValue } from './max';
import { calculateSumValue } from './sum';
import {
  BasicSqlColumnName,
  SqlTableName,
  ThetaSqlColumnName,
  WorkloadSqlColumnName,
} from '../db-persistent/constants';
import { PreloadDbObject, PreloadSecondaryDbObject } from '../data-model/memDataStructure';
import { setApiState } from '../state';
import { getArbitraryForMemoryDB, getArbitraryWithoutDataForMemoryDB } from './arbitrary';


export const populateMemoryDB = async (sendMessage = console.log) => {
  const { timeSeries, theta } = SqlTableName;
  const res = await tablesExist([timeSeries, theta]);
  if (res) {
    sendMessage('populateMemoryDB started...');
    await populate();
    sendMessage('populateMemoryDB done!');
  } else {
    sendMessage('ERROR: tables are missing');
    setApiState('ERROR');
  }
};


const populate = async () => {
  try {
    setApiState('CALCULATING');

    const { timeSeries, theta } = SqlTableName;
    const { damInflux } = BasicSqlColumnName;
    const { thetaA, thetaE } = ThetaSqlColumnName;

    const years = (await getTimestampsOfSecondDayOfYear(timeSeries))
      .map(row => getYearWithLastZeroMinute(row.timestamp)).filter(onlyUnique);

    await asyncForEach(Object.entries(WorkloadSqlColumnName), async (sqlColumnName) => {
      const [columnNameKey, columnNameString] = sqlColumnName;
      await updateOrInsert.onDemand({
        type: <keyof typeof WorkloadSqlColumnName>columnNameKey,
        years: await getColumnGroupedByYears(years, timeSeries, columnNameString),
      });
    });

    await updateOrInsert.onDemand({
      type: 'damInflux',
      years: await getColumnGroupedByYears(years, timeSeries, damInflux),
    });

    await updateOrInsert.onDemand({
      type: 'solarDirectA',
      years: await calculateSolarDirect(years, 'a'),
    });

    await updateOrInsert.onDemand({
      type: 'solarDiffuseA',
      years: await calculateSolarDiffuse(years, 'a'),
    });

    await updateOrInsert.onDemand({
      type: 'solarDirectB',
      years: await calculateSolarDirect(years, 'b'),
    });

    await updateOrInsert.onDemand({
      type: 'solarDiffuseB',
      years: await calculateSolarDiffuse(years, 'b'),
    });

    await updateOrInsert.onDemand({
      type: 'solarDirectC',
      years: await calculateSolarDirect(years, 'c'),
    });

    await updateOrInsert.onDemand({
      type: 'solarDiffuseC',
      years: await calculateSolarDiffuse(years, 'c'),
    });

    await updateOrInsert.onDemand({
      type: 'wind',
      years: await calculateWind(years),
    });

    await updateOrInsert.arbitrary(await getArbitraryForMemoryDB());
    const arbitraryWithoutData = await getArbitraryWithoutDataForMemoryDB();

    const preloadData: PreloadDbObject['data'] = {
      lowResSectionDataCollection: await calculateLowRes(years, arbitraryWithoutData),
      maxSectionDataCollection: await calculateMaxValue(years, arbitraryWithoutData),
      sumSectionDataCollection: await calculateSumValue(years, arbitraryWithoutData),
      availableYearsInApi: years,
      arbitraryWithoutData: arbitraryWithoutData,
    };

    await updateOrInsert.preload(preloadData);

    const preloadSecondaryData: PreloadSecondaryDbObject['data'] = {
      thetaA: await getSingleColumnAsArray(theta, thetaA),
      thetaE: await getSingleColumnAsArray(theta, thetaE),
    };

    await updateOrInsert.preloadSecondary(preloadSecondaryData);

    setApiState('OK');

  } catch (e) {
    setApiState('ERROR');
    throw e;
  }
};
