import { reduceOnDemandSections } from './helpers';
import { ArbitraryDbObjectWithoutData } from '../data-model/memDataStructure';

const maxReducer = (max: number, val: number) => {
  return Math.max(max, val);
};


export const calculateMaxValue = async (years: number[], arbitrary: ArbitraryDbObjectWithoutData[]) => {
  return await reduceOnDemandSections(years, arbitrary, maxReducer, () => 0);
};
