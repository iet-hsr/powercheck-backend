import { groupByYears, mergeResult } from './helpers';
import { CompleteRow, getColumnsWithout29FebPartial } from '../db-persistent/read';
import {
  BasicSqlColumnName,
  SolarDiffuseSqlColumnName,
  SolarDirectSqlColumnName,
  SqlTableName,
} from '../db-persistent/constants';
import { ByYearOrSlug } from '../data-model/memDataStructure';

type Weights = { [key in keyof typeof SolarDirectSqlColumnName]: number }
type WeightsCollection = {
  a: Weights;
  b: Weights;
  c: Weights
}

const weights = {
  // Equal weighted
  a: {
    altd: 1,
    ande: 1,
    badr: 1,
    bant: 1,
    base: 1,
    bisc: 1,
    buff: 1,
    evio: 1,
    fahy: 1,
    frib: 1,
    genf: 1,
    goes: 1,
    gron: 1,
    meir: 1,
    pull: 1,
    robi: 1,
    stab: 1,
    uetl: 1,
    visp: 1,
    zerm: 1,
  },
  // Bevölkerungsgewichtet
  b: {
    altd: 0.96,
    ande: 0.1,
    badr: 0.44,
    bant: 2,
    base: 1.32,
    bisc: 1.82,
    buff: 0.08,
    evio: 0.58,
    fahy: 0.26,
    frib: 1.16,
    genf: 1.4,
    goes: 1.64,
    gron: 0.32,
    meir: 0.28,
    pull: 1.6,
    robi: 0.08,
    stab: 0.44,
    uetl: 5.14,
    visp: 0.32,
    zerm: 0.08,
  },
  // Alpen und Alpensüdseite
  c: {
    altd: 0,
    ande: 0,
    badr: 0,
    bant: 0,
    base: 0,
    bisc: 0,
    buff: 1,
    evio: 0,
    fahy: 0,
    frib: 0,
    genf: 0,
    goes: 0,
    gron: 0,
    meir: 0,
    pull: 0,
    robi: 1,
    stab: 1,
    uetl: 0,
    visp: 0,
    zerm: 1,
  },
};

function weightedAverage(
  version: keyof WeightsCollection,
  row: { [key in SolarDirectSqlColumnName | SolarDiffuseSqlColumnName]: number },
  ColumnNames: typeof SolarDiffuseSqlColumnName | typeof SolarDirectSqlColumnName,
) {
  // Gewichtetes Mittel der 20 Solarmessstandorte bilden
  // -> Durchschnittliche Einstrahlung auf horizontale Platte (in W/m2) P

  const totalWeight = Object.values(weights[version]).reduce((prev, val) => prev + val, 0);

  return Object.entries(weights[version]).reduce(
    (prev, [region, weight]) => prev + row[ColumnNames[<keyof Weights>region]] * (weight / totalWeight),
    0,
  );
}

type PartialSqlTable = (Pick<CompleteRow, BasicSqlColumnName.timeStamp | SolarDirectSqlColumnName> |
  Pick<CompleteRow, BasicSqlColumnName.timeStamp | SolarDiffuseSqlColumnName>)[];

const calculateSolarSpecific = async (
  years: number[],
  ColumnNames: typeof SolarDiffuseSqlColumnName | typeof SolarDirectSqlColumnName,
  version: keyof WeightsCollection,
) => {
  const result: ByYearOrSlug<number[]> = {};
  const rowBatchSize = 100000;
  let startRow = 0;

  while (true) {
    const partialSqlTable = <PartialSqlTable>await getColumnsWithout29FebPartial(
      SqlTableName.timeSeries,
      [
        BasicSqlColumnName.timeStamp,
        ...Object.values(ColumnNames),
      ],
      startRow,
      rowBatchSize,
    );

    if (partialSqlTable.length <= 0) {
      break;
    }

    const solarValuesByYear = groupByYears(
      years,
      partialSqlTable,
    );

    const averageOfAllLocationsBatch = Object.entries(solarValuesByYear).reduce<ByYearOrSlug<number[]>>((partialResult, [year, rows]) => {
      // Runs for each year
      partialResult[parseInt(year)] = [];
      for (let i = 0; i < rows.length; i++) {
        let averageOfAllLocations = weightedAverage(version, <{ [key in SolarDirectSqlColumnName | SolarDiffuseSqlColumnName]: number }>rows[i], ColumnNames);
        partialResult[parseInt(year)].push(averageOfAllLocations);
      }
      return partialResult;
    }, {});

    mergeResult(result, averageOfAllLocationsBatch);

    startRow += rowBatchSize;
  }

  return result;
};


export const calculateSolarDirect = async (years: number[], version: keyof WeightsCollection) => {
  return calculateSolarSpecific(years, SolarDirectSqlColumnName, version);
};

export const calculateSolarDiffuse = async (years: number[], version: keyof WeightsCollection) => {
  return calculateSolarSpecific(years, SolarDiffuseSqlColumnName, version);
};
