import { ApiState } from './data-model/sharedTypes';

let apiState: ApiState;
setApiState('CALCULATING');

export function setApiState(newState: ApiState) {
  console.log(`New API State: ${newState}`);
  apiState = newState;
}

export const getApiState = () => apiState;
