import { populateMemoryDB } from './business-logic/computeLocalData';
import * as authController from './api/controllers/authController';
import express from 'express';
import http from 'http';
import path from 'path';
import bodyParser from 'body-parser';
import socketIo from 'socket.io';
import { sqlDataStructureArray } from './data-model/sqlDataStructure';
import apiRouter from './api/routes/dataRoutes';
import adminApiRouter from './api/routes/adminDataRoutes';
import { adminApiStateCheck, apiStateCheck } from './api/middleware/apiStateCheck';
import { initializeSqlDb } from './db-persistent/initializeSqlDb';

let app = express(),
  httpServer = http.createServer(app),
  port = process.env.PORT || 5000,
  io = socketIo(httpServer);

export const sendMessage = (message: string) => {
  console.log(`send message: ${message}`);
  io.emit('message', message);
};

const setupHandlebars = () => {
  app.set('views', path.join(__dirname, '../src/views'));
  app.set('view engine', 'hbs');
};

const serveFrontends = () => {
  // Serve the admin frontend (pw protected cvs upload)
  app.get('/admin', authController.adminAuth, function (req, res) {
    res.render('admin', { fileNames: sqlDataStructureArray.map(table => table.name) });
  });
};

app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));

initializeSqlDb().then(() => {
  setupHandlebars();
  serveFrontends();
  app.use('/api', apiStateCheck, apiRouter);
  app.use('/admin-api', adminApiStateCheck, adminApiRouter);

  httpServer.listen(port, () => {
    console.log('RESTful API server started on: ' + port);
  });

  populateMemoryDB().catch((e) => {
    console.log(e);
  });
});
