export enum SpecialSection {
  preload = 'preload',
  preloadSecondary = 'preloadSecondary',
}

export enum OnDemandSection {
  endUser = 'endUser',
  import = 'import',
  export = 'export',
  nuclear = 'nuclear',
  thermal = 'thermal',
  river = 'river',
  damInflux = 'damInflux',
  solarDirectA = 'solarDirectA',
  solarDiffuseA = 'solarDiffuseA',
  solarDirectB = 'solarDirectB',
  solarDiffuseB = 'solarDiffuseB',
  solarDirectC = 'solarDirectC',
  solarDiffuseC = 'solarDiffuseC',
  wind = 'wind',
}
