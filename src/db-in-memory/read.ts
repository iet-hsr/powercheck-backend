import db from './index';
import {
  ArbitraryDbObject, ArbitrarySectionName,
  DbObject,
  OnDemandDbObject,
  OnDemandSectionName,
  PreloadDbObject,
} from '../data-model/memDataStructure';
import express from 'express';
import { SpecialSection } from './constants';

export const findAndSend = (query: Partial<DbObject>, res: express.Response) => {
  db.findOne(query, (err, document) => {
    if (err) {
      res.send(err);
    } else {
      res.json(document);
    }
  });
};

export const getOnDemand = async (type: OnDemandSectionName, year: number): Promise<OnDemandDbObject> => {
  return new Promise((resolve, reject) => {
    db.findOne({
      type,
      year,
    }, ((err, document) => {
      if (err) {
        reject(err);
      } else {
        resolve(document);
      }
    }));
  });
};

export const getArbitrary = async (type: ArbitrarySectionName, slug: string): Promise<ArbitraryDbObject> => {
  return new Promise((resolve, reject) => {
    db.findOne({
      type,
      slug,
    }, ((err, document) => {
      if (err) {
        reject(err);
      } else {
        resolve(document);
      }
    }));
  });
};

export const getPreload = async (): Promise<PreloadDbObject> => {
  return new Promise((resolve, reject) => {
    db.findOne({
      type: SpecialSection.preload,
    }, ((err, document) => {
      if (err) {
        reject(err);
      } else {
        resolve(document);
      }
    }));
  });
};
