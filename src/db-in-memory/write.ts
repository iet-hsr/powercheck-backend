import db from './index';
import { asyncForEach } from '../business-logic/helpers';
import {
  ArbitraryDbObject,
  ArbitraryQuery,
  ArbitrarySectionName,
  DbObject,
  OnDemandDbObject,
  OnDemandQuery,
  OnDemandSectionName,
  PreloadDbObject,
  PreloadQuery,
  PreloadSecondaryDbObject,
  PreloadSecondaryQuery,
  Query,
} from '../data-model/memDataStructure';
import { SpecialSection } from './constants';


const _updateOrInsertOnDemand = async (data: number[], type: OnDemandSectionName, year: number) => {
  const queryObject: OnDemandQuery = {
    type,
    year,
  };
  const dataObject: OnDemandDbObject = {
    ...queryObject,
    data,
  };
  return _updateOrInsert(queryObject, dataObject);
};

const _updateOrInsertArbitrary = async (
  data: number[],
  type: ArbitrarySectionName,
  slug: string,
  order: number,
) => {
  const queryObject: ArbitraryQuery = {
    type,
    slug,
  };
  const dataObject: ArbitraryDbObject = {
    ...queryObject,
    data,
    order,
  };
  return _updateOrInsert(queryObject, dataObject);
};

const _updateOrInsertPreload = async (data: PreloadDbObject['data']) => {
  const queryObject: PreloadQuery = {
    type: SpecialSection.preload,
  };
  const dataDbObject: PreloadDbObject = {
    ...queryObject,
    data,
  };
  return _updateOrInsert(queryObject, dataDbObject);
};


const _updateOrInsertPreloadSecondary = async (data: PreloadSecondaryDbObject['data']) => {
  const queryObject: PreloadSecondaryQuery = {
    type: SpecialSection.preloadSecondary,
  };
  const dataDbObject: PreloadSecondaryDbObject = {
    ...queryObject,
    data,
  };
  return _updateOrInsert(queryObject, dataDbObject);
};


const _updateOrInsert = async (queryObject: Query, dataObject: DbObject) => (
  new Promise((resolve, _) => {
    const callback = (err: any, numReplaced: number) => {
      if (numReplaced === 0) {
        db.insert(dataObject, () => {
          resolve();
        });
      } else {
        resolve();
      }
    };
    db.update(queryObject, dataObject, {}, callback);
  })
);

export type MultiYearEntry = {
  type: OnDemandSectionName;
  years: {
    [key: number]: number[];
  };
}

export const updateOrInsert = {
  preload: _updateOrInsertPreload,
  preloadSecondary: _updateOrInsertPreloadSecondary,
  onDemand: async (dataObject: MultiYearEntry) => {
    await asyncForEach(Object.entries(dataObject.years), async ([year, data]) => {
      await _updateOrInsertOnDemand(data, dataObject.type, parseInt(year));
    });
  },
  arbitrary: async (dataArray: ArbitraryDbObject[]) => {
    await asyncForEach(dataArray, async ({ slug, order, type, data }) => {
      await _updateOrInsertArbitrary(data, type, slug, order);
    });
  },
};

