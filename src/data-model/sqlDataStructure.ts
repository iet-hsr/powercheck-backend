import {
  BasicSqlColumnName,
  SolarDiffuseSqlColumnName,
  SolarDirectSqlColumnName,
  SqlColumnName,
  SqlTableName,
  SqlType,
  ThetaSqlColumnName,
  WindSqlColumnName,
  WorkloadSqlColumnName,
} from '../db-persistent/constants';

const { float, dateTime, key } = SqlType;

export type SqlTableStructure = {
  name: SqlTableName;
  columns: SqlColumnStructure[];
}

export type SqlColumnStructure = {
  name: SqlColumnName;
  type: SqlType;
  quotedValue?: boolean;
}

const SolarDirectColumnNamesOrdered = [
  SolarDirectSqlColumnName.altd,
  SolarDirectSqlColumnName.ande,
  SolarDirectSqlColumnName.badr,
  SolarDirectSqlColumnName.bant,
  SolarDirectSqlColumnName.base,
  SolarDirectSqlColumnName.bisc,
  SolarDirectSqlColumnName.buff,
  SolarDirectSqlColumnName.evio,
  SolarDirectSqlColumnName.fahy,
  SolarDirectSqlColumnName.frib,
  SolarDirectSqlColumnName.genf,
  SolarDirectSqlColumnName.goes,
  SolarDirectSqlColumnName.gron,
  SolarDirectSqlColumnName.meir,
  SolarDirectSqlColumnName.pull,
  SolarDirectSqlColumnName.robi,
  SolarDirectSqlColumnName.stab,
  SolarDirectSqlColumnName.uetl,
  SolarDirectSqlColumnName.visp,
  SolarDirectSqlColumnName.zerm,
];

const SolarDiffuseColumnNamesOrdered = [
  SolarDiffuseSqlColumnName.altd,
  SolarDiffuseSqlColumnName.ande,
  SolarDiffuseSqlColumnName.badr,
  SolarDiffuseSqlColumnName.bant,
  SolarDiffuseSqlColumnName.base,
  SolarDiffuseSqlColumnName.bisc,
  SolarDiffuseSqlColumnName.buff,
  SolarDiffuseSqlColumnName.evio,
  SolarDiffuseSqlColumnName.fahy,
  SolarDiffuseSqlColumnName.frib,
  SolarDiffuseSqlColumnName.genf,
  SolarDiffuseSqlColumnName.goes,
  SolarDiffuseSqlColumnName.gron,
  SolarDiffuseSqlColumnName.meir,
  SolarDiffuseSqlColumnName.pull,
  SolarDiffuseSqlColumnName.robi,
  SolarDiffuseSqlColumnName.stab,
  SolarDiffuseSqlColumnName.uetl,
  SolarDiffuseSqlColumnName.visp,
  SolarDiffuseSqlColumnName.zerm,
];

const WindColumnNamesOrdered = [
  WindSqlColumnName.dole,
  WindSqlColumnName.bier,
  WindSqlColumnName.brev,
  WindSqlColumnName.oron,
  WindSqlColumnName.chau,
  WindSqlColumnName.cres,
  WindSqlColumnName.chas,
  WindSqlColumnName.bant,
  WindSqlColumnName.beat,
  WindSqlColumnName.egol,
  WindSqlColumnName.hild,
  WindSqlColumnName.schm,
  WindSqlColumnName.hoer,
  WindSqlColumnName.chur,
  WindSqlColumnName.aros,
  WindSqlColumnName.valb,
  WindSqlColumnName.grim,
  WindSqlColumnName.leyt,
];

export const sqlDataStructureObject: { [key: string]: SqlTableStructure } = {
  timeSeries: {
    name: SqlTableName.timeSeries,
    columns: [
      {
        name: BasicSqlColumnName.id,
        type: key,
      },
      {
        name: BasicSqlColumnName.timeStamp,
        type: dateTime,
        quotedValue: true,
      },
      {
        name: WorkloadSqlColumnName.endUser,
        type: float,
      },
      {
        name: WorkloadSqlColumnName.import,
        type: float,
      },
      {
        name: WorkloadSqlColumnName.export,
        type: float,
      },
      {
        name: WorkloadSqlColumnName.nuclear,
        type: float,
      },
      {
        name: WorkloadSqlColumnName.thermal,
        type: float,
      },
      ...SolarDirectColumnNamesOrdered.reduce((partial: SqlColumnStructure[], directName: SolarDirectSqlColumnName, index: number) => {
        partial.push({
          name: directName,
          type: float,
        });
        partial.push({
          name: SolarDiffuseColumnNamesOrdered[index],
          type: float,
        });
        return partial;
      }, []),
      ...WindColumnNamesOrdered.map(name => (
        {
          name, type: float,
        }
      )),
      {
        name: WorkloadSqlColumnName.river,
        type: float,
      },
      {
        name: BasicSqlColumnName.damInflux,
        type: float,
      },
    ],
  },
  theta: {
    name: SqlTableName.theta,
    columns: [
      {
        name: ThetaSqlColumnName.id,
        type: SqlType.key,
      },
      {
        name: BasicSqlColumnName.timeStamp,
        type: dateTime,
        quotedValue: true,
      },
      {
        name: ThetaSqlColumnName.thetaA,
        type: SqlType.float,
      },
      {
        name: ThetaSqlColumnName.thetaE,
        type: SqlType.float,
      },
    ],
  },
};

export const sqlDataStructureArray: SqlTableStructure[] = Object.values(sqlDataStructureObject);
