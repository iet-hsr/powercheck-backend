import { OnDemandSection } from '../db-in-memory/constants';
import { ArbitrarySection } from './sharedTypes';

export type OnDemandSectionName = keyof OnDemandMemData;
export type ArbitrarySectionName = keyof ArbitraryMemData;
export type PreloadSectionName = keyof PreloadMemData;
export type PreloadSecondarySectionName = keyof PreloadSecondaryMemData;


export type DbObject = OnDemandDbObject | PreloadDbObject | PreloadSecondaryQuery | ArbitraryDbObject;
export type OnDemandDbObject = {
  type: OnDemandSectionName;
  year: number;
  data: number[];
};
export type ArbitraryDbObject = ArbitraryDbObjectWithoutData & {
  data: number[];
};
export type ArbitraryDbObjectWithoutData = {
  type: ArbitrarySectionName;
  slug: string;
  order: number;
};
export type PreloadDbObject = {
  type: PreloadSectionName;
  data: {
    lowResSectionDataCollection: LowResSectionDataCollection,
    maxSectionDataCollection: MaxSectionDataCollection,
    sumSectionDataCollection: SumSectionDataCollection,
    availableYearsInApi: number[];
    arbitraryWithoutData: ArbitraryDbObjectWithoutData[],
  }
}
export type PreloadSecondaryDbObject = {
  type: PreloadSecondarySectionName;
  data: {
    thetaA: number[],
    thetaE: number[],
  }
}
export type LowResSectionDataCollection = SectionsByYearOrSlug<number[]>;
export type MaxSectionDataCollection = SectionsByYearOrSlug<number>;
export type SumSectionDataCollection = SectionsByYearOrSlug<number>;

export type Query = PreloadQuery | PreloadSecondaryQuery | OnDemandQuery | ArbitraryQuery;
export type PreloadQuery = {
  type: PreloadSectionName;
}
export type PreloadSecondaryQuery = {
  type: PreloadSecondarySectionName;
}
export type OnDemandQuery = {
  type: OnDemandSectionName;
  year: number;
}
export type ArbitraryQuery = {
  type: ArbitrarySectionName;
  slug: string;
}


export type OnDemandMemData = OnDemandSections<OnDemandDbObject>;
export type ArbitraryMemData = ArbitrarySections<ArbitraryDbObject>;
export type PreloadMemData = {
  preload: PreloadDbObject;
}
export type PreloadSecondaryMemData = {
  preloadSecondary: PreloadSecondaryDbObject;
}

export type SectionsByYearOrSlug<T> = OnDemandSections<ByYearOrSlug<T>>;
export type ArbitrarySectionsByYearOrSlug<T> = ArbitrarySections<ByYearOrSlug<T>>;

type OnDemandSections<T> = {
  -readonly [_ in keyof typeof OnDemandSection]: T;
}

type ArbitrarySections<T> = {
  -readonly [_ in keyof typeof ArbitrarySection]: T;
}

export type ByYearOrSlug<T> = {
  [key in (number | string)]: T;
}
