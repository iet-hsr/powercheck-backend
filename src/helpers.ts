export function assertUnreachable(_: never): never {
  throw new Error('Didn\'t expect to get here');
}

export const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
