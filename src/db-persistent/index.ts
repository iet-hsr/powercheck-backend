import { Pool } from 'pg';

let url = process.env.HEROKU_POSTGRESQL_COBALT_URL;

const pool = new Pool({
  connectionString: url,
  ssl: true,
});

export default (query: string) => pool.query(query);
