import sendQuery from './index';
import { asyncForEach } from '../business-logic/helpers';
import {
  ArbitraryDataSqlColumnName,
  ArbitraryDataSqlTableName,
  BasicSqlColumnName,
  SqlTableName,
} from './constants';
import { SqlColumnStructure, sqlDataStructureArray } from '../data-model/sqlDataStructure';
import { ArbitrarySeriesDto, DeleteArbitrarySeriesDto } from '../data-model/sharedTypes';

const QUERY_ROW_LIMIT = 50000;

/**
 * Drops a table and fills it with new data
 * @param tableName Name of the table to be dropped and filled
 * @param desiredColumnsStructure Specifies the generated columns
 * @param rawTableData Data to be inserted into the table. First Row needs to contain column names.
 * @param sendMessage Log function
 */
async function setTableData(
  tableName: SqlTableName,
  desiredColumnsStructure: SqlColumnStructure[],
  rawTableData: RawRowData[],
  sendMessage = console.log,
) {
  // Crate a mapping from the desired column order to the csv column order.
  // Any columns not present in desiredColumnsStructure will not be applied.
  const desiredColumnsToRawDataColumnsMap = new Map<number, number>();

  // Separate header row
  const rawDataHeaderRow = rawTableData.splice(0, 1)[0];

  desiredColumnsStructure.forEach((col, i) => {
    const mapTarget = rawDataHeaderRow.findIndex(caseInsensitiveComparator(col.name));
    if (mapTarget >= 0) {
      desiredColumnsToRawDataColumnsMap.set(i, mapTarget);
    }
  });


  const drop_query = `DROP TABLE IF EXISTS ${tableName}; `;

  const create_query = `CREATE TABLE ${tableName} (${
    desiredColumnsStructure.map((column) => ` ${column.name} ${column.type}`)
  } ); `;

  const query = drop_query + create_query;

  sendMessage(`sending drop & create query: ${query}`);
  const dropCreateRes = await sendQuery(query);
  console.log(dropCreateRes);

  const formatCell = (cell: string, quoted: boolean | undefined) => quoted ? `'${cell}'` : cell;
  const formatRow = (row: string[]) => ` (${
    Array.from(desiredColumnsToRawDataColumnsMap.entries())
      .map(([desiredColumnIndex, rawDataColumnIndex]) => (
        formatCell(
          row[rawDataColumnIndex],
          desiredColumnsStructure[desiredColumnIndex].quotedValue,
        )
      ))
  })`;

  const totalRows = rawTableData.length;
  let insertedRows = 0;
  while (rawTableData.length > 0) {
    const part = rawTableData.splice(0, QUERY_ROW_LIMIT);
    const insert_query = `SET datestyle = "ISO, DMY"; INSERT INTO ${tableName} (${
      Array.from(desiredColumnsToRawDataColumnsMap.keys())
        .map((columnIndex) => ` ${desiredColumnsStructure[columnIndex].name}`)
    } ) VALUES ${part.map((row) => formatRow(row))}; `;

    sendMessage(`inserting ${part.length} rows...`);
    const res = await sendQuery(insert_query);
    insertedRows += part.length;
    console.log(res);
    sendMessage(`successfully inserted ${insertedRows} of ${totalRows} rows`);
  }
}

function caseInsensitiveComparator(str: string) {
  return (item: string) => item.toLowerCase() === str.toLocaleLowerCase();
}

export type RawDataTables = { [key: string]: RawRowData[] }
export type RawRowData = string[];

function verifyColumns(headerRow: string[], tableStructureColumns: SqlColumnStructure[]) {
  tableStructureColumns.forEach((columnStructure) => {
    if (columnStructure.name === BasicSqlColumnName.id) {
      return;
    }
    if (headerRow.findIndex(caseInsensitiveComparator(columnStructure.name)) >= 0) {
      return;
    }
    throw new Error(`Column: ${columnStructure.name} is missing. Required columns: ${
      tableStructureColumns.map(col => col.name)}`);
  });
}

export default async (rawData: RawDataTables, sendMessage = console.log) => {
  const rawDataTableNames = Object.keys(rawData);
  await asyncForEach(rawDataTableNames, async (rawDataTableName: string) => {
    const tableStructure = sqlDataStructureArray.find((table => table.name == rawDataTableName));
    if (!tableStructure) {
      throw new Error(`Data Structure does not define a table named ${rawDataTableName}`);
    }
    verifyColumns(rawData[tableStructure.name][0], tableStructure.columns);
    await setTableData(
      tableStructure.name,
      tableStructure.columns,
      rawData[tableStructure.name],
      sendMessage,
    );
  });
};

export async function insertArbitraryDataSeries(
  dto: ArbitrarySeriesDto,
  sendMessage = console.log,
): Promise<void> {
  const { section, order, slug, dataSeries } = dto;
  const {
    section: sectionColumn,
    order: orderColumn,
    slug: slugColumn,
    dataSeries: dataSeriesColumn,
  } = ArbitraryDataSqlColumnName;
  const query = `
    INSERT INTO ${ArbitraryDataSqlTableName} (
        ${sectionColumn},
        ${orderColumn},
        ${slugColumn},
        ${dataSeriesColumn}
      )
      VALUES (
        '${section}',
        ${order},
        '${slug}',
        '{${dataSeries}}'
      ) ;
  `;

  sendMessage(`send insert query: ${query.length > 400 ? query.substring(0, 390) + '...' : query}`);
  await sendQuery(query);
}

export async function deleteArbitraryDataSeries(
  dto: DeleteArbitrarySeriesDto,
  sendMessage = console.log,
): Promise<void> {
  const { section, slug } = dto;
  const {
    section: sectionColumn,
    slug: slugColumn,
  } = ArbitraryDataSqlColumnName;
  const query = `
    DELETE FROM ${ArbitraryDataSqlTableName}
      WHERE
        ${sectionColumn} = '${section}'
        AND ${slugColumn} = '${slug}'
    ;
  `;

  sendMessage(`send delete query: ${query}`);
  await sendQuery(query);
}
