import sendQuery from './index';
import {
  ArbitraryDataSqlColumnName,
  ArbitraryDataSqlTableName,
  BasicSqlColumnName,
  SqlColumnName,
  SqlTableName,
} from './constants';
import { ArbitrarySection } from '../data-model/sharedTypes';

export type CompleteRow = Omit<{
  [key in SqlColumnName]: number
}, BasicSqlColumnName.timeStamp> & {
  [BasicSqlColumnName.timeStamp]: Date;
}

type Row = Partial<CompleteRow>;

type ArbitraryRowWithoutData = {
  [ArbitraryDataSqlColumnName.section]: ArbitrarySection;
  [ArbitraryDataSqlColumnName.slug]: string;
  [ArbitraryDataSqlColumnName.order]: number;
}

type ArbitraryRow = ArbitraryRowWithoutData & {
  [ArbitraryDataSqlColumnName.dataSeries]: number[];
}

export async function getArbitraryWithoutData(): Promise<ArbitraryRowWithoutData[]> {
  const { section, order, slug } = ArbitraryDataSqlColumnName;
  const query = `
    SELECT  ${section},
            ${slug},
            ${order}
      FROM ${ArbitraryDataSqlTableName};
  `;
  console.log(`sending read query: ${query}`);
  return <ArbitraryRowWithoutData[]>(await sendQuery(query)).rows;
}

export async function getArbitraryWithData(): Promise<ArbitraryRow[]> {
  const { section, order, slug, dataSeries } = ArbitraryDataSqlColumnName;
  const query = `
    SELECT  ${section},
            ${slug},
            ${order},
            ${dataSeries}
      FROM ${ArbitraryDataSqlTableName};
  `;
  console.log(`sending read query: ${query}`);
  return <ArbitraryRow[]>(await sendQuery(query)).rows;
}

export const getColumns = async (
  tableName: SqlTableName,
  columnNames: SqlColumnName[],
  whereQuery = '',
) => {
  const query = `SELECT ${columnNames.join(', ')} FROM ${tableName} ${whereQuery}; `;
  console.log(`sending read query: ${query}`);
  return <Row[]>(await sendQuery(query)).rows;
};

export const getColumnsWithout29Feb = async (
  tableName: SqlTableName,
  columnNames: SqlColumnName[],
) => await getColumns(
  tableName,
  columnNames,
  `WHERE EXTRACT(DAY FROM TIMESTAMP) != 29 OR EXTRACT(MONTH FROM timestamp) != 2`,
);

export const getColumnsWithout29FebPartial = async (
  tableName: SqlTableName,
  columnNames: SqlColumnName[],
  rowStart: number,
  rowLimit: number,
) => await getColumns(
  tableName,
  columnNames,
  `WHERE EXTRACT(DAY FROM TIMESTAMP) != 29 OR EXTRACT(MONTH FROM timestamp) != 2 ORDER BY ${BasicSqlColumnName.id}
  LIMIT ${rowLimit} 
  OFFSET ${rowStart}`,
);

export const getTimestampsOfSecondDayOfYear = async (tableName: SqlTableName) => {
  const query = `SELECT ${BasicSqlColumnName.timeStamp} FROM ${tableName}
WHERE EXTRACT(DOY FROM ${BasicSqlColumnName.timeStamp}) = 2; `;
  console.log(`sending read query: ${query}`);
  return (await sendQuery(query)).rows;
};
