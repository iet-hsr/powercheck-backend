import { ArbitraryDataSqlColumnName, ArbitraryDataSqlTableName, SqlType } from './constants';
import sendQuery from './index';
import { QueryResult } from 'pg';

export async function initializeSqlDb(): Promise<void> {
  await createArbitraryDataTable();
}

async function createArbitraryDataTable(
  sendMessage = console.log,
) {
  const { id, order, section, slug, dataSeries } = ArbitraryDataSqlColumnName;
  const { text, float, floatArray } = SqlType;
  const query = `CREATE TABLE ${ArbitraryDataSqlTableName}
  (
    ${id}         SERIAL,
    ${section}    ${text},
    ${slug}       ${text},
    ${order}      ${float},
    ${dataSeries} ${floatArray}
  ); `;

  sendMessage(`sending create query: ${query}`);
  let dropCreateRes: QueryResult | null = null;
  try {
    dropCreateRes = await sendQuery(query);
  } catch (e) {
    sendMessage(`Create Table failed, it probably already exists: ${e}`);
    return;
  }
  console.log(dropCreateRes);
  return;
}
