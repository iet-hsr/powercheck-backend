import sendQuery from './index';
import { SqlTableName } from './constants';

export const tableExists = async (tableName: SqlTableName) => {
  const query = `SELECT to_regclass('public.${tableName}');`;
  return !!(await sendQuery(query)).rows[0].to_regclass;
};

export const tablesExist = async (tableNames: SqlTableName[]) => {
  for (let i = 0; i < tableNames.length; i++) {
    if (!(await tableExists(tableNames[i]))) {
      return false;
    }
  }
  return true;
};