export enum SqlType {
  float = 'real NOT NULL',
  smallInt = 'SMALLINT NOT NULL',
  key = 'SERIAL NOT NULL',
  dateTime = 'TIMESTAMP NOT NULL',
  text = 'TEXT NOT NULL',
  floatArray = 'real[] NOT NULL',
}

export enum SqlTableName {
  timeSeries = 'timeseries',
  theta = 'theta',
}

export const ArbitraryDataSqlTableName = 'arbitrary_data';

export enum ArbitraryDataSqlColumnName {
  id = 'ID',
  section = 'section',
  slug = 'slug',
  order = 'order_weight',
  dataSeries = 'data_series',
}

export enum ThetaSqlColumnName {
  id = 'ID',
  timeStamp = 'timestamp',
  thetaA = 'theta_a',
  thetaE = 'theta_e',
}

export enum BasicSqlColumnName {
  id = 'ID',
  timeStamp = 'timestamp',
  year = 'year',
  damInflux = 'dam_influx',
}

export enum WorkloadSqlColumnName {
  endUser = 'enduser_workload',
  nuclear = 'nuclear_workload',
  thermal = 'thermal_workload',
  river = 'river_workload',
  import = 'import',
  export = 'export',
}

export enum SolarDirectSqlColumnName {
  altd = 'solar_direct_01_altdorf',
  ande = 'solar_direct_02_andeer',
  badr = 'solar_direct_03_badragaz',
  bant = 'solar_direct_04_bantiger',
  base = 'solar_direct_05_basel',
  bisc = 'solar_direct_06_bischofszell',
  buff = 'solar_direct_07_buffalora',
  evio = 'solar_direct_08_evionnaz',
  fahy = 'solar_direct_09_fahy',
  frib = 'solar_direct_10_fribourg',
  genf = 'solar_direct_11_genf',
  goes = 'solar_direct_12_goesgen',
  gron = 'solar_direct_13_grono',
  meir = 'solar_direct_14_meiringen',
  pull = 'solar_direct_15_pully',
  robi = 'solar_direct_16_robiei',
  stab = 'solar_direct_17_stabio',
  uetl = 'solar_direct_18_uetliberg',
  visp = 'solar_direct_19_visp',
  zerm = 'solar_direct_20_zermatt',
}

export enum SolarDiffuseSqlColumnName {
  altd = 'solar_diffus_01_altdorf',
  ande = 'solar_diffus_02_andeer',
  badr = 'solar_diffus_03_badragaz',
  bant = 'solar_diffus_04_bantiger',
  base = 'solar_diffus_05_basel',
  bisc = 'solar_diffus_06_bischofszell',
  buff = 'solar_diffus_07_buffalora',
  evio = 'solar_diffus_08_evionnaz',
  fahy = 'solar_diffus_09_fahy',
  frib = 'solar_diffus_10_fribourg',
  genf = 'solar_diffus_11_genf',
  goes = 'solar_diffus_12_goesgen',
  gron = 'solar_diffus_13_grono',
  meir = 'solar_diffus_14_meiringen',
  pull = 'solar_diffus_15_pully',
  robi = 'solar_diffus_16_robiei',
  stab = 'solar_diffus_17_stabio',
  uetl = 'solar_diffus_18_uetliberg',
  visp = 'solar_diffus_19_visp',
  zerm = 'solar_diffus_20_zermatt',
}

export enum WindSqlColumnName {
  dole = 'wind_01_dole',
  bier = 'wind_02_biere',
  brev = 'wind_03_brevine',
  oron = 'wind_04_oron',
  chau = 'wind_05_chauxdefonds',
  cres = 'wind_06_cressier',
  chas = 'wind_07_chasseral',
  bant = 'wind_08_bantiger', // Achtung diese Station ist die einzige bereits in 100m höhe. Sie wird per Index in der Berechnung von der höhenkorrektur ausgenommen.
  beat = 'wind_09_beatenberg',
  egol = 'wind_10_egolzwil',
  hild = 'wind_11_hildisrieden',
  schm = 'wind_12_schmerikon',
  hoer = 'wind_13_hoernli',
  chur = 'wind_14_chur',
  aros = 'wind_15_arosa',
  valb = 'wind_16_valbella',
  grim = 'wind_17_grimsel',
  leyt = 'wind_18_leytron',
}

export type SqlColumnName
  = BasicSqlColumnName
  | ThetaSqlColumnName
  | WindSqlColumnName
  | SolarDirectSqlColumnName
  | SolarDiffuseSqlColumnName
  | WorkloadSqlColumnName
  | ArbitraryDataSqlColumnName;

export default {};
