import express from 'express';
import { readData, readDataSeries, readLatestDataSeries } from '../controllers/dataController';

const router = express.Router();

router.get('/get/:type/latest', readLatestDataSeries);

router.get('/get/:type/:yearOrSlug', readDataSeries);

router.get(`/get/:type`, readData);

export default router;
