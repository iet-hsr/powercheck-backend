import express from 'express';
import { process_file, save_file } from '../controllers/dataController';
import { adminAuth } from '../controllers/authController';
import {
  deleteAdminArbitraryDataSeries,
  postAdminArbitraryDataSeries,
  postAdminCalculationRequest,
  readAdminOnDemandDataSeries,
} from '../controllers/adminController';

const router = express.Router();

router.post('/upload/', adminAuth, save_file, process_file);
router.get('/get/on-demand/', adminAuth, readAdminOnDemandDataSeries);
router.post(
  '/upload/arbitrary/',
  adminAuth,
  postAdminArbitraryDataSeries,
  readAdminOnDemandDataSeries,
);
router.delete(
  '/delete/arbitrary/',
  adminAuth,
  deleteAdminArbitraryDataSeries,
  readAdminOnDemandDataSeries,
);
router.post(
  ''
  + '/calculate-db/',
  adminAuth,
  postAdminCalculationRequest,
  readAdminOnDemandDataSeries,
);

export default router;
