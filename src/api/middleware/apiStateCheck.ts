import { NextFunction, Request, Response } from 'express';
import { getApiState } from '../../state';
import { assertUnreachable } from '../../helpers';

export const apiStateCheck = function (req: Request, res: Response, next: NextFunction) {
  const apiState = getApiState();
  switch (apiState) {
    case 'CALCULATING':
      res.status(503).send('Api not ready. Please wait');
      break;
    case 'ERROR':
      res.status(500).send('Backend error');
      break;
    case 'OK':
    case 'OK_CALCULATION_NEEDED':
      next();
      break;
    default:
      assertUnreachable(apiState);
  }
};

export const adminApiStateCheck = function (req: Request, res: Response, next: NextFunction) {
  const apiState = getApiState();
  switch (apiState) {
    case 'CALCULATING':
      res.status(503).send('Api not ready. Please wait');
      break;
    case 'ERROR':
    case 'OK':
    case 'OK_CALCULATION_NEEDED':
      next();
      break;
    default:
      assertUnreachable(apiState);
  }
};
