import auth from 'basic-auth';
import express, { NextFunction } from 'express';

// TODO: Delete this for production:
if (!process.env.ADMIN_PW) {
  process.env.ADMIN_PW = '1234';
}

const admins: { [key: string]: { password: string; } } = {
  admin: { password: process.env.ADMIN_PW },
};

export const adminAuth = function (request: express.Request, response: express.Response, next: NextFunction) {
  let user = auth(request);
  if (!user || !admins[user.name] || admins[user.name].password !== user.pass) {
    response.set('WWW-Authenticate', 'Basic realm="Infos im README.md"');
    return response.status(401).send('Authentication Failed');
  }
  return next();
};
