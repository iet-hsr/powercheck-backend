import create from '../../db-persistent/write';
import { populateMemoryDB } from '../../business-logic/computeLocalData';
import { sendMessage } from '../../server';
import { findAndSend, getPreload } from '../../db-in-memory/read';
import express, { NextFunction } from 'express';
import { OnDemandSection, SpecialSection } from '../../db-in-memory/constants';
import { isInEnumKeys } from '../../business-logic/helpers';
import { sqlDataStructureArray } from '../../data-model/sqlDataStructure';
import multer from 'multer';
import Papa, { ParseConfig, ParseError, ParseResult } from 'papaparse';
import * as fs from 'fs';
import { setApiState } from '../../state';
import { DbObject } from '../../data-model/memDataStructure';
import { ArbitrarySection } from '../../data-model/sharedTypes';

const upload = multer({ dest: 'tmp/csv/' });

export const readDataSeries = function (req: express.Request, res: express.Response) {
  const rawType = req.params.type;
  let rawYearOrSlug: string | number = req.params.yearOrSlug;
  const year: number = parseInt(rawYearOrSlug, 10);
  let query: Partial<DbObject>;
  if (isNaN(year)) {
    if (!Object.values(ArbitrarySection).includes(rawType as ArbitrarySection)){
      res.status(400).send('unknown series');
    }
    query = {
      type: rawType as ArbitrarySection,
      slug: req.params.yearOrSlug,
    }
  } else {
    if (!Object.values(OnDemandSection).includes(rawType as OnDemandSection)){
      res.status(400).send('unknown series');
    }
    query = {
      type: rawType as OnDemandSection,
      year: year,
    }
  }
  findAndSend(query, res);
};

export const readLatestDataSeries = function (req: express.Request, res: express.Response) {
  getPreload().then((preloadObject) => {
    const type = req.params.type;
    if (isInEnumKeys(type, OnDemandSection)) {
      const query = {
        type: type,
        year: preloadObject.data.availableYearsInApi[0],
      };
      findAndSend(query, res);
    }
  }).catch((err) => {
    res.send(err);
  });
};

export const readData = function (req: express.Request, res: express.Response) {
  const type = req.params.type;
  if (isInEnumKeys(type, OnDemandSection) || isInEnumKeys(type, SpecialSection)) {
    const query = {
      type: type,
    };
    findAndSend(query, res);
  } else {
    res.send('Error: Unknown type');
  }
};

export const save_file = (req: express.Request, res: express.Response, next: NextFunction) => {
  sendMessage('file upload started...');

  const uploadInstance = upload.fields(
    sqlDataStructureArray.map((table) => ({
      name: table.name,
      maxCount: 1,
    })),
  );
  uploadInstance(req, res, next);
};


export const process_file = function (req: express.Request & { files: any }, res: express.Response) {
  const nrOfTables = sqlDataStructureArray.length;
  let doneCounter = 0;
  const data: { [key: string]: any } = {};
  sendMessage('file upload done!');
  res.send('OK');

  const processData = () => {
    try {
      create(data, sendMessage).then(() => {
        populateMemoryDB(sendMessage).then(() => {
          sendMessage('File processing done. All OK!');
        });
      }).catch((e) => {
        sendMessage(`SQL INSERT Failed! ${e}`);
      });

    } catch (e) {
      res.send(e);
    }
  };

  const parseDataFromFile = (filepath: string, fileRows: any[], tableName: string) => {
    setApiState('CALCULATING');
    sendMessage(`reading file ${tableName}`);

    const onSuccess = (results: ParseResult): void => {
      fileRows = results.data;
      fs.unlinkSync(filepath);   // remove temp file
      doneCounter += 1;
      data[tableName] = fileRows;
      if (doneCounter >= nrOfTables) {
        processData();
      }
    };

    const onError = (error: ParseError): void => {
      fs.unlinkSync(filepath);   // remove temp file
      sendMessage(error.message);
      res.send(error);
    };

    const config: ParseConfig = {
      header: false,
      complete: onSuccess,
      error: onError,
      delimitersToGuess: [',', ';'],
    };

    const file = fs.createReadStream(filepath);
    Papa.parse(file, config);
  };

  for (let table of sqlDataStructureArray) {
    let { name } = table;
    if (req.files[name]) {
      const fileRows: any[] = [];
      let filepath = req.files[name][0].path;
      parseDataFromFile(filepath, fileRows, name);
    } else {
      doneCounter += 1;
    }
  }
};
