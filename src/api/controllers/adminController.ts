import express from 'express';
import {
  AdminStuffDto,
  ArbitrarySection,
  ArbitrarySeriesDto,
  isArbitrarySeriesDto,
  isDeleteArbitrarySeriesDto,
  isValidSlug,
  SectionWithoutData,
} from '../../data-model/sharedTypes';
import { getApiState, setApiState } from '../../state';
import { getArbitraryWithoutData } from '../../db-persistent/read';
import { deleteArbitraryDataSeries, insertArbitraryDataSeries } from '../../db-persistent/write';
import { populateMemoryDB } from '../../business-logic/computeLocalData';

export function readAdminOnDemandDataSeries(
  req: express.Request,
  res: express.Response,
) {
  getSections()
    .then((sections) => {
      const dto: AdminStuffDto = {
        apiState: getApiState(),
        sections,
      };
      res.send(dto);
    })
    .catch((err) => {
      res.send(err);
    });
}

async function isUnique(
  dto: ArbitrarySeriesDto,
): Promise<boolean> {
  getSections().then((sections) => {
    sections.forEach((section) => {
      section.arbitrarySeries.forEach((series) => {
        if (series.slug === dto.slug) {
          return false;
        }
      });
    });
  });
  return true;
}

export function postAdminArbitraryDataSeries(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) {
  if (!isArbitrarySeriesDto(req.body)) {
    res.status(400).send('bad dto format');
    return;
  }
  const dto = req.body;
  if (!isValidSlug(dto.slug)) {
    res.status(400)
      .send('slug must begin with a letter and can only contain letters, number and _');
    return;
  }
  if (!isUnique(dto)) {
    res.status(400).send('already exists');
    return;
  }
  insertArbitraryDataSeries(dto).then(() => {
    setApiState('OK_CALCULATION_NEEDED');
    next();
  }).catch((e) => {
    res.status(500).send(e);
    return;
  });
}


export function deleteAdminArbitraryDataSeries(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) {
  if (!isDeleteArbitrarySeriesDto(req.body)) {
    res.status(400).send('bad dto format');
    return;
  }
  const dto = req.body;
  if (!isValidSlug(dto.slug)) {
    res.status(400)
      .send('slug must begin with a letter and can only contain letters, number and _');
    return;
  }
  deleteArbitraryDataSeries(dto).then(() => {
    setApiState('OK_CALCULATION_NEEDED');
    next();
  }).catch((e) => {
    res.status(500).send(e);
    return;
  });
}


export function postAdminCalculationRequest(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) {
  setApiState('CALCULATING');
  populateMemoryDB().then();
  next();
}


async function getSections(): Promise<SectionWithoutData[]> {
  const rows = await getArbitraryWithoutData();
  const sections: { [key in string]: SectionWithoutData } = {};
  rows.forEach(({ order_weight, section, slug }) => {
    const series = { order: order_weight, slug };
    sections[section] === undefined
      ? sections[section] = { name: section, measuredSeries: [], arbitrarySeries: [series] }
      : sections[section].arbitrarySeries.push(series);
  });
  Object.values(ArbitrarySection).forEach((section) => {
    if (sections[section] === undefined) {
      sections[section] = { name: section, measuredSeries: [], arbitrarySeries: [] };
    }
  });
  return Object.values(sections);
}
